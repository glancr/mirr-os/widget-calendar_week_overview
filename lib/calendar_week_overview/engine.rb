# frozen_string_literal: true

module CalendarWeekOverview
  class Engine < ::Rails::Engine
    isolate_namespace CalendarWeekOverview
    config.generators.api_only = true
  end
end
