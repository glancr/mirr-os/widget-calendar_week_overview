# frozen_string_literal: true

require 'json'
$LOAD_PATH.push File.expand_path('lib', __dir__)

# Maintain your gem's version:
require 'calendar_week_overview/version'

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = 'calendar_week_overview'
  spec.version     = CalendarWeekOverview::VERSION
  spec.authors     = ['Tobias Grasse']
  spec.email       = ['tg@glancr.de']
  spec.homepage    = 'https://glancr.de'
  spec.summary     = 'mirr.OS widget that displays multiple calendars in a table overview.'
  spec.description = 'Shows your calendar events for the next 7 days, one column for each calendar. Perfect for families!'
  spec.license     = 'MIT'
  spec.metadata    = { 'json' =>
    {
      type: 'widgets',
      title: {
        enGb: 'This Week',
        deDe: 'Woche',
        frFr: 'Semaine',
        esEs: 'Semana',
        plPl: 'Tydzień',
        koKr: '주별 개요'

      },
      description: {
        enGb: spec.description,
        deDe: 'Zeigt deine Kalendereinträge der nächsten 7 Tage, eine Spalte pro Kalender. Perfekt für Familien!',
        frFr: 'Affiche vos événements de calendrier pour les 7 prochains jours, une colonne pour chaque calendrier. Parfait pour les familles!',
        esEs: 'Muestra sus eventos de calendario para los próximos 7 días, una columna para cada calendario. Perfecto para familias!',
        plPl: 'Pokazuje wydarzenia w kalendarzu na następne 7 dni, po jednej kolumnie dla każdego kalendarza. Idealny dla rodzin!',
        koKr: '다음 7 일 동안의 캘린더 일정을 각 캘린더마다 하나씩 표시합니다. 가족에게 딱!'
      },
      group: 'calendar',
      compatibility: '1.0.0',
      sizes: [
        { w: 8, h: 4 },
        { w: 12, h: 4 },
        { w: 8, h: 6 },
        { w: 12, h: 6 }
      ],
      single_source: false
    }.to_json }

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise 'RubyGems 2.0 or newer is required to protect against ' \
      'public gem pushes.'
  end

  spec.files = Dir['{app,config,db,lib}/**/*', 'MIT-LICENSE', 'Rakefile', 'README.md']
  spec.add_development_dependency 'rails', '~> 5.2'
  spec.add_development_dependency 'rubocop'
  spec.add_development_dependency 'rubocop-rails'
end
