# frozen_string_literal: true

module CalendarWeekOverview
  class Configuration < WidgetInstanceConfiguration
    attribute :show_current_events, :boolean, default: false

    validates :show_current_events, boolean: true
  end
end
